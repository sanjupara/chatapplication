import './App.css'
import { Navbar } from './components/Navbar'
import { Chat } from './components/Chat';
import { BrowserRouter, Routes, Route } from "react-router-dom"
import { People } from './components/People';
import Login from './pages/login';
import { Message } from './components/Message';

function App() {

  return (
    <>
      <BrowserRouter>
          <Navbar />
            <Routes>
              <Route path="/login" element={<Login/>} />
              <Route path="/" element={<People/>} />
              <Route path="/:username" element={<Chat/>} />
            </Routes>
        </BrowserRouter>  

    </> 
  )
}

export default App;
