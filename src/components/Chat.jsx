import '../styles/Chat.css';
import { Message } from './Message';
import { People } from './People';

export function Chat() {
  return (
    <div className="chat-container">
      <People />
      <Message />
    </div>
  );
}
