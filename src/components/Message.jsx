import React, { useEffect, useState } from 'react';
import axios from 'axios';
import { useParams } from 'react-router-dom';
import '../styles/Message.css';
import '../styles/Chat.css';

export function Message() {



  const [messages, setMessages] = useState([]);
  const [newMessage, setNewMessage] = useState('');
  const { username } = useParams(); // Assuming you use the username in the route path

  useEffect(() => {
    const fetchMessages = async () => {
      try {
        // Assuming you have the user ID in session storage
        const myUserName = sessionStorage.getItem('userName');

        const response = await axios.get(`http://localhost:3000/messages?users=${myUserName}&usersTwo=${username}`);
        setMessages(response.data);
        console.log(username)
      } catch (error) {
        console.error('Error fetching messages:', error);
      }
    };

    fetchMessages();
  }, [username]);

  const handleSendMessage = async () => {
    if (newMessage.trim() === '') {
      return;
    }

    try {
      // Assuming you have the user ID in session storage
      const myUserName = sessionStorage.getItem('userName');

      const response = await axios.post('http://localhost:3000/messages', {
        message: newMessage,
        users: [myUserName, username],
        timestamp: new Date().toISOString(),
        read: false,
      });

      setMessages([...messages, response.data]);
      setNewMessage('');
    } catch (error) {
      console.error('Error sending message:', error);
    }
  };

  const formatTimestamp = (timestamp) => {
    const date = new Date(timestamp);
    const hours = date.getHours().toString().padStart(2, '0');
    const minutes = date.getMinutes().toString().padStart(2, '0');
    return `${hours}:${minutes}`;
  };

  return (
    <div className="message-container">
      <div className="messages">
        {messages.map((message) => (
          <div key={message?.id} className={`message-box ${message && message.read ? 'read' : 'unread'}`}>
            {message && (
              <div className="message-content">
                <p>{message.message}</p>
                <div className="message-footer">
                  <p className="timestamp">{formatTimestamp(message.timestamp)}{message.users}</p>
                </div>
              </div>
            )}
          </div>
        ))}
      </div>
      <div className="new-message-box">
        <textarea
          placeholder="Type your message..."
          value={newMessage}
          onChange={(e) => setNewMessage(e.target.value)}
        />
        <button onClick={handleSendMessage}>Send</button>
      </div>
    </div>
  );
}
