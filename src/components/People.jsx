// People.js

import React, { useEffect, useState } from 'react';
import { Link } from 'react-router-dom';
import '../styles/People.css';
import '../styles/Chat.css';
import { useNavigate } from 'react-router-dom';
import video from '../media/wah.mp4';
import video2 from '../media/delv.mp4';

export function People() {
  const navigate = useNavigate();
  const [users, setUsers] = useState([]);

  const handleLogout = () => {
    // Clear session storage
    sessionStorage.clear();
    // Redirect to login page
    navigate('/login');
  };

  useEffect(() => {
    fetch('http://localhost:3000/persons')
      .then((res) => res.json())
      .then((data) => setUsers(data))
      .catch((error) => console.error('Error fetching data:', error));
  }, []); 

  const formatTime = (date) => {
    return new Date(date).toLocaleString(undefined, { hour: 'numeric', minute: 'numeric' });
  };

  return (
    <>
      <div className="people-container">
        {users.map((user) => (
          <Link key={user.id} to={`/${user.name}`} className="person-box-link">
            <div className="person-box">
              <div className="person">
                <h2 className="person-name">{user.name}</h2>
                <p className="last-online">Last online: {formatTime(user.lastOnline)}</p>
              </div>
            </div>
          </Link>
        ))}
      </div>
      
      </>

  );
}
