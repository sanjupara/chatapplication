import React, { useState } from 'react';
import { useNavigate } from 'react-router-dom';
import '../styles/Navbar.css';

export function Navbar() {
  const navigate = useNavigate();
  const [isSearchInputVisible, setIsSearchInputVisible] = useState(false);
  const [searchTerm, setSearchTerm] = useState('');

  const handleSearchClick = () => {
    setIsSearchInputVisible(true);
  };

  const handleSearchClose = () => {
    setIsSearchInputVisible(false);
    setSearchTerm('');
  };

  const handleSearchChange = (e) => {
    setSearchTerm(e.target.value);
    // You can perform search-related logic here
  };

  const handleGoBack = () => {
    navigate('/');
  };

  const handleLogout = () => {
    // Clear session storage
    sessionStorage.clear();
    // Redirect to login page
    navigate('/login');
  };

  return (
    <ul>
      <li onClick={handleGoBack}>Go Back</li>
      <li onClick={handleSearchClick}>Search</li>
      <li onClick={handleLogout}>Log out</li>

      <div className="search-box">
        {isSearchInputVisible && (
          <>
            <input
              type="text"
              placeholder="Search..."
              value={searchTerm}
              onChange={handleSearchChange}
            />
            <button onClick={handleSearchClose}>Close</button>
          </>
        )}
      </div>
    </ul>
  );
}
