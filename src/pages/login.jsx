import React, { useState } from 'react';
import { useNavigate } from 'react-router-dom';
import '../styles/Login.css'; // Create a Login.css file for styling

function Login() {
  const [name, setName] = useState('');
  const [password, setPassword] = useState('');
  const navigate = useNavigate();

  const handleLogin = () => {
    navigate('/');
  };

  const handleNameChange = (event) => {
    setName(event.target.value);
  };

  const handlePasswordChange = (event) => {
    setPassword(event.target.value);
  };

  const handleSubmit = async (event) => {
    event.preventDefault();

    try {
      const response = await fetch('http://localhost:3000/persons', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({
          name,
          password,
        }),
      });

      const data = await response.json();

      console.log('User created:', data);

      // Clear textboxes
      setName('');
      setPassword('');

      // Store user ID in sessionStorage
      sessionStorage.setItem('userName', data.name);
      handleLogin();
    } catch (error) {
      console.error('Error creating user:', error);
    }
  };

  return (
    <div className="login-container">
      <h1 className="san-chat">SanChat</h1>

      <p className="welcome-text">
        Welcome to SanChat! A chat application that allows you to chat with your friends and family.
      </p>

      <form className="login-form" onSubmit={handleSubmit}>
        <input
          type="text"
          value={name}
          onChange={handleNameChange}
          placeholder="Enter your name"
          className="name-input"
        />
        <input
          type="password"
          value={password}
          onChange={handlePasswordChange}
          placeholder="Enter your password"
          className="password-input"
        />
        <button type="submit">Login</button>
      </form>
    </div>
  );
}

export default Login;
