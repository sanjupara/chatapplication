# SanChat

## Description

this is a chat app created with react and expressjs. As database I used mongodb and mongoose. It was part of a project in school. The user can create a new user and login with it. After that he can chat with any existing user. The chat is realtime and the messages are saved in the database. 
In this project I learned how to use a database with mongoose. I also learned to make a fullstack app with react and expressjs.

## Getting Started

If you want to try out the calculator you can download the code and run it on your local machine.

### Prerequisites

You need some things to run the calculator.
* npm
  ```sh
  npm install npm@latest -g
  ```

### Installation

_Below is an example of how you can instruct your audience on installing and setting up your app. This template doesn't rely on any external dependencies or services._

1. Clone the repo
   
        git clone https://gitlab.com/sanjupara/chatapplication

2. Move into directory
   
        cd chatapp
   
3. Install NPM packages
    
        npm install

4. Open in vscode
    
        code .
    
5. Start the App
        
            npm run dev 


But now you also need to clone the backend repo and start it. You can find it here: https://gitlab.com/sanjupara/chatapplicationbackend
Here you do the same steps as before. You can even use the same command to start the app. But you need to start the backend first and then the frontend.
Altough you can't use the app because you don't have the database. If you want to use the app you need to create a database on mongodb and change the connection string in the backend. You can find the connection string in the file server.js. After that you need to create a .env file in the backend and add the following line:
    
        JWT_SECRET=yourSecret

## Usage

Now you can use the app. You can create a new user and login with it. After that you can chat with any existing user. The chat is realtime and the messages are saved in the database.

## Contact

- Project Link: [https://gitlab.com/sa74/versicherung](https://gitlab.com/sa74/versicherung)
- Email: sananjayan.paramanantharajah@lernende.bbw.ch 




